﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using System.ComponentModel;
using WpfApplication2.Model;
using System.IO;
using Newtonsoft.Json;

namespace WpfApplication2.ViewModel
{
    class PaymentsViewModel : INotifyPropertyChanged
    {
        #region fields
        private const string FileName = @"Files/Payments.json";
        private ObservableCollection<Payment> _Payments=new ObservableCollection<Payment>();
        private Payment _SelectedPayment;
        private double _SumAmount;
        private DateTime _FirstDate=new DateTime(DateTime.Now.Year,DateTime.Now.Month,1);
        private DateTime _SecondDate=new DateTime(DateTime.Now.Year, DateTime.Now.Month,DateTime.DaysInMonth(DateTime.Now.Year,DateTime.Now.Month));
        private string _NameFilter="";
        private DateTime _NewDate=DateTime.Now;
        private string _NewName;
        private double _NewAmount;
        private Command _AddCommand;
        #endregion

        #region properties
        public ObservableCollection<Payment> Payments
        {
            get
            {
                var result = _Payments;

                result = new ObservableCollection<Payment>(_Payments.Where(p => p.Date.Date >= FirstDate.Date & p.Date.Date <= SecondDate.Date));

                if (NameFilter != "")
                    result = new ObservableCollection<Payment>(result.Where(p => p.Name == NameFilter));

                return new ObservableCollection<Payment>(result.OrderByDescending(p=>p.Date));
            }
        }
        public Payment SelectedPayment
        {
            get
            {
                return _SelectedPayment;
            }
            set
            {
                _SelectedPayment = value;
                OnPropertyChanged("SelectedPayment");
            }
        }
        public double SumAmount
        {
            get
            {
                _SumAmount = Payments.Sum(p => p.Amount);
                return _SumAmount;
            }           
        }
        public DateTime FirstDate
        {
            get
            {
                return _FirstDate;
            }
            set
            {
                _FirstDate = value;
                OnPropertyChanged("FirstDate");
                OnPropertyChanged("Payments");
            }
        }
        public DateTime SecondDate
        {
            get
            {
                return _SecondDate;
            }
            set
            {
                _SecondDate = value;
                OnPropertyChanged("SecondDate");
                OnPropertyChanged("Payments");
            }
        }
        public string NameFilter
        {
            get
            {
                return _NameFilter;
            }
            set
            {
                _NameFilter = value;
                OnPropertyChanged("NameFilter");
                OnPropertyChanged("Payments");
            }
        }
        public string NewName
        {
            get
            {
                return _NewName;
            }
            set
            {
                if (value.Length == 0 | value.Length > 20)
                    throw new FormatException();
                _NewName = value;
            }
        }
        public DateTime NewDate
        {
            get
            {
                return _NewDate;
            }
            set
            {
                _NewDate = value;
            }
        }
        public double NewAmount
        {
            get
            {
                return _NewAmount;
            }
            set
            {
                if (value<=0)
                    throw new FormatException();

                _NewAmount = value;
            }
        }
        public Command AddCommand
        {
            get
            {
                return _AddCommand ??
                (_AddCommand = new Command(obj =>
                {
                    Payment p = new Payment(NewName, NewDate, NewAmount);
                    AddPayment(p);        
                },
                (obj) => (NewName!=null & NewDate!=null & NewAmount!=0)));
        }
        }
        #endregion

        #region methods
        public PaymentsViewModel()
        {
   
            var payments = Read();
            foreach (var p in payments)
                AddPayment(p);
             
            PropertyChanged += (s, a) =>
            {
                if (a.PropertyName == "Payments")
                    OnPropertyChanged("SumAmount");
            };
          
        }
        public void AddPayment(Payment p)
        {
            _Payments.Add(p);
            p.PropertyChanged += (s, a) =>
              {
                  if (a.PropertyName == "Amount")
                      OnPropertyChanged("SumAmount");
                  if (a.PropertyName == "Date" | a.PropertyName == "Date")
                      OnPropertyChanged("Payments");
              
              };
            OnPropertyChanged("Payments");
        }
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        private ObservableCollection<Payment> Read()
        {

            try
            {
                var payments = File.ReadAllText(FileName, Encoding.UTF8);
                return JsonConvert.DeserializeObject<ObservableCollection<Payment>>(payments, new JsonSerializerSettings() { Formatting = Formatting.Indented });
            }
            catch(FileNotFoundException)
            {
                File.Create(FileName);
                return new ObservableCollection<Payment>();
            }           
            
        }
        private void Write()
        {
            var payments = JsonConvert.SerializeObject(_Payments,Formatting.Indented);
            File.WriteAllText(FileName,payments, Encoding.UTF8);                  
        }
        ~PaymentsViewModel()
        {
            Write();
        }
        #endregion

        #region events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

    }
}
