﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace WpfApplication2.Model
{
    class Payment : INotifyPropertyChanged
    {
        #region fields
        string _Name;
        DateTime _Date;
        double _Amount;        
        #endregion

        #region properties
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (value.Length > 50)
                    throw new FormatException();
                _Name = value;
                OnPropertyChanged("Name");
            }
        }

        public DateTime Date
        {
            get
            {
                return _Date;
            }
            set
            {
              _Date = value;
                OnPropertyChanged("Date");
            }
        }

        public double Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                if (value <= 0)
                    throw new FormatException();

                _Amount = value;
                OnPropertyChanged("Amount");
            }
        }
        #endregion

        #region methods
        public Payment()
        {
        }
        public Payment(string n,DateTime d, double a)
        {
            Name = n;
            Date = d;
            Amount = a;
        }
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        #region events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion
    }
}
